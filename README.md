# Shokas/php-docker-laravel

Docker image crafted for running Laravel CI pipelines.

## Built-in applications

* [GIT](https://git-scm.com/)
* [PHP](http://php.net) (from official [PHP Docker images](https://registry.hub.docker.com/_/php/))
* [NodeJS](https://nodejs.org)
	* [Yarn](https://yarnpkg.com/)
	* [Npm](http://npmjs.com/)
* [Composer](https://getcomposer.org)
* [PHP_CodeSniffer](https://www.squizlabs.com/php-codesniffer) 
* [PHPUnit](https://phpunit.de)

### PHP

* PHP 7.3

### Composer

* Composer is installed globally.
* Composer command example: `composer self-update`.

### PHP_CodeSniffer

* PHP_CodeSniffer is installed globally.
* Command example: `phpcs --standard=PSR2 --extensions=php`.

### PHPUnit

* PHPUnit is installed globally.
* PHPUnit example `phpunit --verbose --coverage-text --colors=never`.

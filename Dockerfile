FROM php:7.3-fpm

MAINTAINER Žilvinas Gadliauskas <zilvinas@gadliauskas.lt>

RUN apt-get update
RUN apt-get install -y build-essential \
    libssl-dev \
    libcurl4-gnutls-dev \
    libexpat1-dev \
    gettext \
    unzip

# Prod dependencies
RUN apt-get install -y bash \
    curl \
    g++ \
    gcc \
    git \
    imagemagick \
    libc-dev \
    libpng-dev \
    make \
    mariadb-client \
    nodejs \
    npm \
    yarn \
    openssh-client \
    rsync \
    zlib1g-dev \
    libzip-dev \
    libxml2-dev

# PHP extensions
RUN docker-php-ext-configure zip --with-libzip
RUN docker-php-ext-install \
    curl \
    iconv \
    mbstring \
    pdo \
    pdo_mysql \
    pcntl \
    tokenizer \
    xml \
    gd \
    exif \
    zip \
    bcmath

# Composer
ENV COMPOSER_HOME /composer
ENV PATH ./vendor/bin:/composer/vendor/bin:$PATH
ENV COMPOSER_ALLOW_SUPERUSER 1
RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer

# PHPUnit
RUN curl -OL https://phar.phpunit.de/phpunit.phar \
	&& chmod 755 phpunit.phar \
	&& mv phpunit.phar /usr/local/bin/ \
	&& ln -s /usr/local/bin/phpunit.phar /usr/local/bin/phpunit

ADD php.ini /usr/local/etc/php/conf.d/docker-php.ini

# PHP_CodeSniffer
RUN composer global require "squizlabs/php_codesniffer=*"

# Working directory
WORKDIR /var/www
